package com.example.navigation

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NavUtils
import androidx.core.app.TaskStackBuilder

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        supportActionBar?.apply {
            title = "Second Activity"
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                navigationUp()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun navigationUp() {
        val upIntent: Intent? = NavUtils.getParentActivityIntent(this)
        when {
            upIntent == null -> throw IllegalStateException("No Parent Activity Intent")
            NavUtils.shouldUpRecreateTask(this, upIntent) -> {
                TaskStackBuilder.create(this)
                    .addNextIntentWithParentStack(upIntent)
                    .startActivities()
            }
            else -> {
                NavUtils.navigateUpTo(this, upIntent)
            }
        }
    }
}
