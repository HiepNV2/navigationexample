package com.example.navigation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavDeepLinkBuilder
import androidx.navigation.fragment.NavHostFragment
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_open_fragment.setOnClickListener {
            val directions = if (edt_name.text.isNullOrEmpty()) {
                MainFragmentDirections.actionMainFragmentToDestinationFragment()
            } else {
                MainFragmentDirections.actionMainFragmentToDestinationFragment(edt_name.text.toString())
            }
            NavHostFragment.findNavController(this).navigate(directions)
        }

        btn_open_activity.setOnClickListener {

            val directions = MainFragmentDirections.actionSecondActivity()
            NavHostFragment.findNavController(this).navigate(directions)

        }
    }
}
